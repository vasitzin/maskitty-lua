local TITLE = "maskitty-lua";
local VERSION = "1.0.0";

--[[
  Build a map of hexadecimal symbols.
]]
local charset = {}
charset[0] = "0"
charset[1] = "1"
charset[2] = "2"
charset[3] = "3"
charset[4] = "4"
charset[5] = "5"
charset[6] = "6"
charset[7] = "7"
charset[8] = "8"
charset[9] = "9"
charset[10] = "A"
charset[11] = "B"
charset[12] = "C"
charset[13] = "D"
charset[14] = "E"
charset[15] = "F"

--[[
  Get a table's lenght
]]
function table_length(table)
  local ln = 0
  for _ in pairs(table) do ln = ln + 1 end
  return ln
end

--[[
  Split the option string on delimeter.
]]
function split(option, delimeter)
  local res = {}
  for str in string.gmatch(option, "([^"..delimeter.."]+)") do
    table.insert(res, str)
  end
  return res
end

--[[
  Expand a number range of N-M format.
]]
function expand(splits)
  local res = {}
  for _,v in pairs(splits) do
    local found = string.find(v, '-', 1, true)
    if found ~= nil then
      local start = tonumber(string.sub(v, 1, found - 1));
      local stop = tonumber(string.sub(v, found + 1, string.len(v)))
      for num=start,stop do
        table.insert(res, num)
      end
    else
      table.insert(res, tonumber(v))
    end
  end
  return res
end

--[[
  Get the max of expanded range.
]]
function get_max(cores)
  local max = cores[1]
  for _,v in pairs(cores) do
    if v > max then
      max = v
    end
  end
  return max
end

--[[
  Parse cmd options string, get value.
]]
function get_cmd_option(arg, soption, loption)
  local cmd = ""
  for _,v in pairs(arg) do
    if string.find(v, soption, 1, true) ~= nil
    or string.find(v, loption, 1, true) ~= nil then
      local eq = string.find(v, '=', 1, true)
      cmd = string.sub(v, eq + 1, string.len(v))
      return cmd
    end
  end
  return cmd
end

--[[
  Parse cmd options string, get bool of existance.
]]
function is_cmd_option(arg, soption, loption)
  for _,v in pairs(arg) do
    if string.find(v, soption, 1, true) ~= nil
    or string.find(v, loption, 1, true) ~= nil then
      return true
    end
  end
  return false
end

--[[
  Calculate the codes for the cores specified.
]]
function calc_codes(cores)
  local codes = {}
  local prev = 1
  table.insert(codes, prev)
  for i=1,cores do
    prev = 2 * prev
    table.insert(codes, prev)
  end
  return codes
end

--[[
  Convert a decimal integer to hexadecimal string.
]]
function int_to_hex(mask)
  local mask_str = ""
  local q = mask;
  local r;
  while (q > 15) do
    local q_old = q;
    q = math.floor(q / 16);
    r = q_old % 16;
    mask_str = charset[r] .. mask_str;
  end
  if q ~= 0 then
    mask_str = charset[q] .. mask_str;
  end
  return mask_str
end

if is_cmd_option(arg, "-h", "--help") then
  print(TITLE ..  " v" .. VERSION .. "\n")
  print("-m=/--mask= [RANGE]\tthe range of nums representing your affinity mask (eg 0-3,6)")
  print("-t/--threads\t\treturn number of threads in output eg 'maskitty -m=0-5 -t' returns '6 3F'")
  print("-h/--help\t\tprint this menu")
  return 0
end

local mask_arg = get_cmd_option(arg, "-m=", "--mask=")
local mask_range = expand(split(mask_arg, ','))
local codes = calc_codes(get_max(mask_range))

local mask_dec = 0
for _,v in pairs(mask_range) do mask_dec = mask_dec + codes[v + 1] end

if is_cmd_option(arg, "-t", "--threads") then io.write(table_length(mask_range) .. " ") end
io.write(int_to_hex(mask_dec) .. "\n")
